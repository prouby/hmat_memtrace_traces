# -*- mode: org -*-
#+STARTUP: indent


#+BEGIN_SRC shell
  # Exec
  ./pseudoBEM --from-file ../tools/points/0.02_1.00_4.00_cylindre.txt     \
              --cprec                                                     \
              --compress-rhs                                              \
              --no-dump                                                   \
              --use-ldlt                                                  \
              --nrhs 1000
  # Extrace traces files
  starpu_fxt_tool -i /tmp/prof_<user>_0
#+END_SRC
