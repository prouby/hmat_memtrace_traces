# -*- mode: org -*-
#+STARTUP: indent


#+BEGIN_SRC shell
  # Exec
  ./pseudoBEM --from-file ../tools/points/0.05_1.00_4.00_cylindre.txt     \
              --cprec                                                     \
              --compress-rhs                                              \
              --no-dump                                                   \
              --use-ldlt                                                  \
              --nrhs 1000
  # Extrace traces files
  starpu_fxt_tool -i /tmp/prof_<user>_0

  # Simulation on previous trace
  $STARPU_DIR/tools/starpu_replay      \
  --memtrace-dir <memtrace/proba/mat/path> --data-rec data.rec tasks.rec

  # Extrace simulation traces
  starpu_fxt_tool -i /tmp/prof_<user>_0
#+END_SRC
